var should = require('chai').should(),
    expect = require('chai').expect,
    supertest = require('supertest'),
    api = supertest('https://reqres.in/api');

    //User API Testcases
    describe('User', function(){
        it('should return a 200 response', function (done) {
            api.get('/users/2')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.data).to.have.property("first_name");
                    expect(res.body.data).to.have.property("last_name");
                    expect(res.body.data).to.have.property("avatar");
                    expect(res.body.data.first_name).to.equal("Janet");
                    done();
                });
        });
        it('should return 404 if user is not found', function (done){
            api.get('/users/80')
                .set('Accept', 'application/json')
                .expect(404)
                .end(function (err, res) {
                    done();
                });
        });
        it('should return list of all users on page number two', function (done) {
            api.get('/users?page=2')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body).to.have.property("page");
                    expect(res.body.page).to.equal(2);
                    expect(res.body.page).to.not.equal(null);
                    done();
                });
        });
        it('Return 201 status code when new user is added', function(done){
            api.post('/users')
                .set('Accept', 'application/x-www-form-urlencoded')
                .send({
                    name: "morpheus",
                    job: "leader"
                })
                .expect('Content-Type', /json/)
                .expect(201)
                .end(function (err, res) {
                    expect(res.body).to.have.property("createdAt");
                    expect(res.body.name).to.equal("morpheus");
                    done();
                });
        });
        it('Should be updated with new name & role', function(done){
            api.put('/users/2')
            .set('Accept', 'application/x-www-form-urlencoded')
            .send({
                name: "Kevin Lion",
                job: "Manager"
            })
            .expect(200)
            .end(function (err, res) {
                expect(res.body.name).to.equal("Kevin Lion");
                expect(res.body.job).to.equal("Manager");
                done();
            });
        });
        it('Verify user is getting deleted successfully', function(done){
            api.delete('/users/2')
               .set('Accept', 'application/json')
               .expect(204)
               .end(function (err, res) {
                done();
            });
        });
        it('User response delayed by 3ms', function(done){
            api.get('/users?delay=3')
            .set('Accept', 'application/json')
            .expect(200)
                    .end(function (err, res) {
                        expect(res.body).to.have.property("page");
                        expect(res.body.page).to.equal(1);
                        expect(res.body.page).to.not.equal(null);
                        expect(res.body.total_pages).to.equal(4);
                        expect(res.body.total).to.equal(12);
                        console.log(res.body);
                        console.log(res.statusCode);
                        done();
                    });
        });
    });   