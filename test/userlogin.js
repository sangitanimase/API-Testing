//Login API Testcases
 var should = require('chai').should(),
 expect = require('chai').expect,
 supertest = require('supertest'),
 api = supertest('https://reqres.in/api');

 describe('Login', function(){
    it('Return the token once successfully logged In', function(){
        api.post('/login')
            .set('Accept', 'application/x-www-form-urlencoded')
            .send({
                email: "peter@klaven",
                password: "cityslicka"
            })
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
                console.log(res.body);
            });
    });

    it('Verify unsuccessful login & return status code 400', function(){
        api.post('/login')
            .set('Accept', 'application/x-www-form-urlencoded')
            .send({
                email: "peter@klaven",
            })
            .expect('Content-Type', /json/)
            .expect(400)
            .end(function (err, res) {
                expect(res.body).to.have.property("error");
                expect(res.body.error).to.not.equal(null);
                expect(res.body.error).to.equal("Missing password");
            });
    });

});